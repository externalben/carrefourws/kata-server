package com.carrefour.kata.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Notification {
    String code;
    String message;
}
