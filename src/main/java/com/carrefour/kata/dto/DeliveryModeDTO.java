package com.carrefour.kata.dto;

import com.carrefour.kata.validation.ValidDeliveryMode;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class DeliveryModeDTO {
    @NotEmpty(message = "delivery name should not be empty")
    @NotBlank(message = "delivery name should not be empty")
    @ValidDeliveryMode
    private String name;
    private String description;
    private List<String> slots;
}
