package com.carrefour.kata.service;

import com.carrefour.kata.dto.DeliveryModeDTO;
import com.carrefour.kata.dto.Notification;
import com.carrefour.kata.mapper.DeliveryModeMapper;
import com.carrefour.kata.model.DeliveryMode;
import com.carrefour.kata.repository.DeliveryModeRepository;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DeliveryModeServiceImpl implements DeliveryModeService {
    private final DeliveryModeRepository deliveryModeRepository;
    private final DeliveryModeMapper deliveryModeMapper;

    public DeliveryModeServiceImpl(DeliveryModeRepository deliveryModeRepository, DeliveryModeMapper deliveryModeMapper) {
        this.deliveryModeRepository = deliveryModeRepository;
        this.deliveryModeMapper = deliveryModeMapper;
    }

    @Override
    public DeliveryModeDTO findByName(String name) {
        DeliveryMode mode = deliveryModeRepository.findByName(name);
        return deliveryModeMapper.toDTO(mode);
    }

    @Override
    public List<DeliveryModeDTO> findAll() {
        List<DeliveryMode> modes = deliveryModeRepository.findAll();
        return modes.stream().map(deliveryModeMapper::toDTO).collect(Collectors.toList());
    }

    @Override
    public Notification confirmDelivery(String mode, String date, String selectedTime) {
        // Validate date format (dd/MM/yyyy)
        if (!isValidDateFormat(date)) {
            return Notification.builder()
                    .code("INVALID_DATE_FORMAT")
                    .message("Invalid date format. Please use dd/MM/yyyy format.")
                    .build();
        }

        // Validate time format (HH:mm)
        if (!isValidTimeFormat(selectedTime)) {
            return Notification.builder()
                    .code("INVALID_TIME_FORMAT")
                    .message("Invalid time format. Please use HH:mm format.")
                    .build();
        }
        // Find the delivery mode based on the given mode name
        DeliveryMode deliveryMode = deliveryModeRepository.findByName(mode);
        // Check if the delivery mode exists
        if (deliveryMode == null) {
            return Notification.builder()
                    .code("INVALID_DELIVERY_MODE")
                    .message("Invalid delivery mode.")
                    .build();
        }
        List<String> timeSlots = this.deliveryModeMapper.toDTO(deliveryMode).getSlots();
        for (String slot : timeSlots) {
            String[] slotTimes = slot.split(":");
            int startTime = Integer.parseInt(slotTimes[0]);
            int endTime = Integer.parseInt(slotTimes[1]);
            LocalTime time = LocalTime.parse(selectedTime);
            int hour = time.getHour();
            // Check if the provided time falls within the slot
            if (hour >= startTime && hour < endTime) {
                return Notification.builder().code("SLOT_CONFIRMED").message("Slot confirmed for " + date + " from " + startTime + "h to " + endTime + "h.").build();
            }
        }

        return Notification.builder().code("SLOT_UNAVAILABLE").message("Please choose another time slot within 9h00 and 17h00").build();
    }

    private boolean isValidDateFormat(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setLenient(false);
        try {
            Date parsedDate = sdf.parse(date);
            return parsedDate != null;
        } catch (ParseException e) {
            return false;
        }
    }

    private boolean isValidTimeFormat(String time) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        sdf.setLenient(false);
        try {
            Date parsedTime = sdf.parse(time);
            return parsedTime != null;
        } catch (ParseException e) {
            return false;
        }
    }
}
