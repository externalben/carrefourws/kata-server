package com.carrefour.kata.service;

import com.carrefour.kata.dto.DeliveryModeDTO;
import com.carrefour.kata.dto.Notification;

import java.util.List;

public interface DeliveryModeService {
    DeliveryModeDTO findByName(String name);

    List<DeliveryModeDTO> findAll();
    Notification confirmDelivery(String mode, String date, String time);
}
