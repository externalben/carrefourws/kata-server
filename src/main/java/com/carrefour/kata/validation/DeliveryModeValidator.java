package com.carrefour.kata.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.Arrays;
import java.util.List;

public class DeliveryModeValidator implements ConstraintValidator<ValidDeliveryMode, String> {
    private static final List<String> VALID_DELIVERY_MODES = Arrays.stream(DeliveryModeEnum.values()).map(Enum::toString).toList();

    @Override
    public void initialize(ValidDeliveryMode constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value != null && VALID_DELIVERY_MODES.contains(value.toUpperCase());
    }
}
