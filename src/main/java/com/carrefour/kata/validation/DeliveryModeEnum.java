package com.carrefour.kata.validation;

public enum DeliveryModeEnum {
    DRIVE,
    DELIVERY,
    DELIVERY_TODAY,
    DELIVERY_ASAP
}
