package com.carrefour.kata.repository;

import com.carrefour.kata.model.DeliveryMode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliveryModeRepository extends JpaRepository<DeliveryMode, Long> {
    DeliveryMode findByName(String name);
}
