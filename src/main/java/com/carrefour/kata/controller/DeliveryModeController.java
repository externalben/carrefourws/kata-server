package com.carrefour.kata.controller;

import com.carrefour.kata.dto.DeliveryModeDTO;
import com.carrefour.kata.dto.Notification;
import com.carrefour.kata.service.DeliveryModeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/api/delivery")
public class DeliveryModeController {
    private final DeliveryModeService deliveryModeService;


    public DeliveryModeController(DeliveryModeService deliveryModeService) {
        this.deliveryModeService = deliveryModeService;
    }
    @Operation(summary = "Returns the delivery mode according to entered mode name")
    @GetMapping("/{name}")
    public ResponseEntity<DeliveryModeDTO> chooseDeliveryMode(@Valid @PathVariable String name) {
        DeliveryModeDTO mode = deliveryModeService.findByName(name);
        if (mode != null) {
            return ResponseEntity.ok(mode);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "Returns the list of delivery modes")
    @GetMapping("/modes")
    public ResponseEntity<List<DeliveryModeDTO>> getAllDeliveryModes() {
        List<DeliveryModeDTO> modes = deliveryModeService.findAll();
        return ResponseEntity.ok(modes);
    }
    @GetMapping("/confirm/{mode}")
    @Operation(summary = "Validate and cofirm delivery and time slot")
    public ResponseEntity<Notification> confirmDelivery(@Valid @PathVariable String mode, @RequestParam String date, @RequestParam String time) {
        Notification notification = this.deliveryModeService.confirmDelivery(mode, date, time);
        return ResponseEntity.ok(notification);
    }
}
