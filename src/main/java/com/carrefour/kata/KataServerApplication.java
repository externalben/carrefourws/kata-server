package com.carrefour.kata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KataServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(KataServerApplication.class, args);
	}

}
