package com.carrefour.kata.mapper;

import com.carrefour.kata.dto.DeliveryModeDTO;
import com.carrefour.kata.model.DeliveryMode;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface DeliveryModeMapper {
    @Mapping(target = "slots", source = "slots", qualifiedBy = StringToStringListMapper.class)
    DeliveryModeDTO toDTO(DeliveryMode deliveryMode);
    @Mapping(target = "slots", source = "slots", qualifiedBy = StringListToStringMapper.class)
    @Mapping(target = "id", ignore = true) // Ignore mapping id from DTO to entity
    DeliveryMode toEntity(DeliveryModeDTO deliveryModeDTO);

    @StringListToStringMapper
    default String mapStringListToString(List<String> stringList) {
        return stringList.stream().collect(Collectors.joining("|")); // Convert List<String> to pipe-separated string
    }

    @StringToStringListMapper
    default List<String> mapStringToStringList(String string) {
        return Arrays.asList(string.split("\\|")); // Convert pipe-separated string to List<String>
    }
}
