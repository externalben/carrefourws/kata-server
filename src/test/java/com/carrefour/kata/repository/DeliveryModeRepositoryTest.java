package com.carrefour.kata.repository;

import com.carrefour.kata.model.DeliveryMode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
public class DeliveryModeRepositoryTest {
    @Autowired
    private DeliveryModeRepository deliveryModeRepository;

    @Test
    public void testFindByName() {
        // Given
        DeliveryMode deliveryMode = new DeliveryMode();
        deliveryMode.setName("Test Mode");
        deliveryMode.setDescription("Test Description");
        deliveryMode.setSlots("9:11|11:13|13:15|15:17");
        deliveryModeRepository.save(deliveryMode);

        // When
        DeliveryMode foundDeliveryMode = deliveryModeRepository.findByName("Test Mode");

        // Then
        assertEquals(deliveryMode.getName(), foundDeliveryMode.getName());
        assertEquals(deliveryMode.getDescription(), foundDeliveryMode.getDescription());
        assertEquals(deliveryMode.getSlots(), foundDeliveryMode.getSlots());
    }
}
