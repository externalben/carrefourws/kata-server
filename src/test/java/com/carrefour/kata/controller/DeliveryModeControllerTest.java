package com.carrefour.kata.controller;

import com.carrefour.kata.dto.DeliveryModeDTO;
import com.carrefour.kata.dto.Notification;
import com.carrefour.kata.service.DeliveryModeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DeliveryModeControllerTest {
    private DeliveryModeService deliveryModeService;
    private DeliveryModeController deliveryModeController;

    @BeforeEach
    public void setUp() {
        deliveryModeService = mock(DeliveryModeService.class);
        deliveryModeController = new DeliveryModeController(deliveryModeService);
    }

    @Test
    public void testChooseDeliveryMode_ValidName() {
        // Given
        String name = "Test Mode";
        DeliveryModeDTO modeDTO = DeliveryModeDTO.builder().name(name).build();
        when(deliveryModeService.findByName(name)).thenReturn(modeDTO);

        // When
        ResponseEntity<DeliveryModeDTO> response = deliveryModeController.chooseDeliveryMode(name);

        // Then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(modeDTO, response.getBody());
    }

    @Test
    public void testChooseDeliveryMode_InvalidName() {
        // Given
        String name = "Invalid Mode";
        when(deliveryModeService.findByName(name)).thenReturn(null);

        // When
        ResponseEntity<DeliveryModeDTO> response = deliveryModeController.chooseDeliveryMode(name);

        // Then
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test
    public void testGetAllDeliveryModes() {
        // Given
        DeliveryModeDTO mode1 = DeliveryModeDTO.builder()
                .name("Mode 1")
                .description("Description 1")
                .slots(Arrays.asList("9:11", "11:13"))
                .build();
        DeliveryModeDTO mode2 = DeliveryModeDTO.builder()
                .name("Mode 2")
                .description("Description 2")
                .slots(Arrays.asList("13:15", "15:17"))
                .build();
        List<DeliveryModeDTO> expectedModes = Arrays.asList(mode1, mode2);
        when(deliveryModeService.findAll()).thenReturn(expectedModes);

        // When
        ResponseEntity<List<DeliveryModeDTO>> response = deliveryModeController.getAllDeliveryModes();

        // Then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedModes, response.getBody());
    }

    @Test
    public void testConfirmDelivery_ValidInput() {
        // Given
        String mode = "Test Mode";
        String date = "13/02/2024";
        String time = "16:00";
        Notification notification = Notification.builder()
                .code("SLOT_CONFIRMED")
                .message("Slot confirmed for 31/12/2022 from 9h to 11h.")
                .build();
        when(deliveryModeService.confirmDelivery(mode, date, time)).thenReturn(notification);

        // When
        ResponseEntity<Notification> response = deliveryModeController.confirmDelivery(mode, date, time);

        // Then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(notification, response.getBody());
    }

    @Test
    public void testConfirmDelivery_InvalidInput() {
        // Given
        String mode = "Invalid Mode";
        String date = "31/02/2023"; // Invalid date
        String time = "15:00";
        Notification notification = Notification.builder()
                .code("INVALID_DATE_FORMAT")
                .message("Invalid date format. Please use dd/MM/yyyy format.")
                .build();
        when(deliveryModeService.confirmDelivery(mode, date, time)).thenReturn(notification);

        // When
        ResponseEntity<Notification> response = deliveryModeController.confirmDelivery(mode, date, time);

        // Then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(notification, response.getBody());
    }
}
