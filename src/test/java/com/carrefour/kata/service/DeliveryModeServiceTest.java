package com.carrefour.kata.service;

import com.carrefour.kata.dto.DeliveryModeDTO;
import com.carrefour.kata.dto.Notification;
import com.carrefour.kata.mapper.DeliveryModeMapper;
import com.carrefour.kata.model.DeliveryMode;
import com.carrefour.kata.repository.DeliveryModeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class DeliveryModeServiceTest {
    @Mock
    private DeliveryModeRepository deliveryModeRepository;

    @Mock
    private DeliveryModeMapper deliveryModeMapper;

    @InjectMocks
    private DeliveryModeServiceImpl deliveryModeService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindByName() {
        // Given
        String name = "Test Mode";
        DeliveryMode mode = new DeliveryMode();
        mode.setName(name);
        DeliveryModeDTO dto = DeliveryModeDTO.builder().name(name).build();
        when(deliveryModeRepository.findByName(name)).thenReturn(mode);
        when(deliveryModeMapper.toDTO(mode)).thenReturn(dto);

        // When
        DeliveryModeDTO result = deliveryModeService.findByName(name);

        // Then
        assertEquals(dto, result);
        verify(deliveryModeRepository, times(1)).findByName(name);
        verify(deliveryModeMapper, times(1)).toDTO(mode);
    }

    @Test
    public void testFindAll() {
        // Given
        List<DeliveryMode> modes = Collections.singletonList(new DeliveryMode());
        List<DeliveryModeDTO> dtos = Collections.singletonList(DeliveryModeDTO.builder().build());
        when(deliveryModeRepository.findAll()).thenReturn(modes);
        when(deliveryModeMapper.toDTO(any())).thenReturn(DeliveryModeDTO.builder().build());

        // When
        List<DeliveryModeDTO> result = deliveryModeService.findAll();

        // Then
        assertEquals(dtos, result);
        verify(deliveryModeRepository, times(1)).findAll();
        verify(deliveryModeMapper, times(1)).toDTO(any());
    }

    @Test
    public void testConfirmDelivery_InvalidDateFormat() {
        // Given
        String mode = "Test Mode";
        String date = "31/02/2022"; // Invalid date format
        String selectedTime = "10:00";

        // When
        Notification notification = deliveryModeService.confirmDelivery(mode, date, selectedTime);

        // Then
        assertEquals("INVALID_DATE_FORMAT", notification.getCode());
        assertEquals("Invalid date format. Please use dd/MM/yyyy format.", notification.getMessage());
    }

    @Test
    public void testConfirmDelivery_InvalidTimeFormat() {
        // Given
        String mode = "Test Mode";
        String date = "31/12/2022";
        String selectedTime = "25:00"; // Invalid time format

        // When
        Notification notification = deliveryModeService.confirmDelivery(mode, date, selectedTime);

        // Then
        assertEquals("INVALID_TIME_FORMAT", notification.getCode());
        assertEquals("Invalid time format. Please use HH:mm format.", notification.getMessage());
    }

    @Test
    public void testConfirmDelivery_InvalidDeliveryMode() {
        // Given
        String mode = "Invalid Mode"; // Mode does not exist
        String date = "31/12/2022";
        String selectedTime = "10:00";

        // Mock behavior of DeliveryModeRepository to return null
        when(deliveryModeRepository.findByName(mode)).thenReturn(null);

        // When
        Notification notification = deliveryModeService.confirmDelivery(mode, date, selectedTime);

        // Then
        assertEquals("INVALID_DELIVERY_MODE", notification.getCode());
        assertEquals("Invalid delivery mode.", notification.getMessage());

        // Verify interaction with the repository
        verify(deliveryModeRepository, times(1)).findByName(mode);
    }

    @Test
    public void testConfirmDelivery_AllValid() {
        // Given
        String modeName = "Valid Mode";
        String slots = "9:11|11:13|13:15|15:17";
        List<String> slotList = List.of(slots.split("\\|"));
        String date = "31/12/2022";
        String selectedTime = "10:00";
        DeliveryMode deliveryMode = new DeliveryMode();
        deliveryMode.setName(modeName);
        deliveryMode.setSlots(slots);

        DeliveryModeDTO dto = DeliveryModeDTO.builder()
                .name(modeName)
                .slots(slotList)
                .build();
        // Assuming the delivery mode exists
        when(deliveryModeRepository.findByName(modeName)).thenReturn(deliveryMode);
        // Assuming the provided time falls within the available slots
        when(deliveryModeMapper.toDTO(deliveryMode)).thenReturn(dto);

        // When
        Notification notification = deliveryModeService.confirmDelivery(modeName, date, selectedTime);

        // Then
        assertEquals("SLOT_CONFIRMED", notification.getCode());
        // Adjust the expected message according to your implementation
        assertEquals("Slot confirmed for 31/12/2022 from 9h to 11h.", notification.getMessage());

        // Verify interactions with the repository and mapper
        verify(deliveryModeRepository, times(1)).findByName(modeName);
        verify(deliveryModeMapper, times(1)).toDTO(deliveryMode);
    }
}
